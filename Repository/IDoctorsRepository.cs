using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
namespace Algoriza_Project_2023BE83.Repository;
public interface IDoctorsRepository
{
    Task<List<Doctorsmodel>> GetAllDoctors();
    Task<Doctorsmodel> GetDoctorById(int id);
    Task<int> AddDoctor(Doctorsmodel doctorModel);
    Task UpdateDoctorById(int id,Doctorsmodel doctorModel);
    Task DeleteDoctorById(int id);
}