 public class Doctorsmodel{
    public int Id {get; set;}
    public required string FirstName {get; set;}
    public required string LastName {get; set;}
    public required string Specialize {get; set;}
    public required string Phone {get; set;}
    public required string Email {get; set;}
    public required string gender {get; set;}
    public required string Image {get; set;}
    public DateOnly Dateofbirth { get; set; }
}